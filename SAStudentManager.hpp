#ifndef SASTUDENT_MANAGER_HPP
#define SASTUDENT_MANAGER_HPP

#include <QLayout>
#include <QFont>
#include <QFontDatabase>
#include <QPushButton>
#include <QPixmap>
#include <QModelIndex>
#include <QIcon>
#include <vector>
#include <qwidget.h>
#include <QToolButton>
#include <QListWidget>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QListWidget>
#include <QComboBox>

#include "include/SQLite/sqlite3.h"
#include <cstdio>

class SAStudentManager : public QWidget{
    Q_OBJECT

    public:

        explicit SAStudentManager(QWidget *parent = nullptr){
            this->setLayout(centralLayout);
            centralLayout->setAlignment(Qt::AlignTop|Qt::AlignLeft);
        }

        void initWidget(){
            classes = importClasses();

            QFontDatabase::addApplicationFont("./fonts/Quicksand-Regular.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-Medium.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-SemiBold.ttf");

            mainRow = new QHBoxLayout;
            ctrlBar = new QHBoxLayout;

            addStudentButton = new QToolButton;

            centralLayout->addLayout(ctrlBar);
            centralLayout->addLayout(studentListLayout);

            //Setup the control bar
            saveButton = new QPushButton;
            classListLabel = new QLabel;
            advancedSettings = new QPushButton;

            saveButton->setText("Speichern");
            saveButton->setStyleSheet("QPushButton{background-color:#ffffff; color: #000000; border: none; border-radius: 5px;}");
            saveButton->setFont(QFont("Quicksand Medium", 10));

            classList->setFont(QFont("Quicksand Regular", 10));
            classList->setStyleSheet("QComboBox{color:black;}");
            
            for(int i=0; i<classes.size(); i++){
                classList->addItem(QString::fromStdString(classes.at(i)));
            }

            classListLabel->setText("Klasse:");
            classListLabel->setStyleSheet("QLabel{color:black;}");
            classListLabel->setFont(QFont("Quicksand Regular", 9));

            advancedSettings->setFont(QFont("Quicksand Regular", 10));
            advancedSettings->setText("Klassen Verwalten");
            advancedSettings->setStyleSheet("QPushButton{background-color:#ffffff; color: #000000; border: none; border-radius: 5px;}");

            ctrlBar->addWidget(classListLabel);
            ctrlBar->addWidget(classList);
            ctrlBar->addWidget(advancedSettings);
            ctrlBar->addWidget(saveButton);

            //Setup the student list
            studentListLayout->addLayout(mainRow);

            mainRow->addWidget(addStudentButton);
            addStudentButton->setText("Neuen Schüler hinzufügen");
            addStudentButton->setFont(QFont("Quicksand Regular", 10));
            addStudentButton->setMaximumSize(250,150);
            addStudentButton->setMinimumSize(250,150);
            addStudentButton->setStyleSheet("QToolButton{background-color: #ffffff; color: #000000; border: none; border-radius: 10px;}");

            connect(addStudentButton, &QToolButton::clicked, this, [=](){
                if(is_row_full(studentListLayout->itemAt(studentListLayout->count())->layout(), 4)){
                    addStudent(mainRow);
                }
            });
            connect(advancedSettings, &QPushButton::clicked, this, &SAStudentManager::manage_class);
        }

        void addStudent(QLayout* addToThisLayout){
            QWidget* student = new QWidget;

            student->setMaximumSize(250,150);
            student->setMinimumSize(250,150);
            student->setStyleSheet("QWidget{background-color: #ffffff; border: none; border-radius: 10px;}");
            
            addToThisLayout->addWidget(student);
        }

        //Check if Row is full
        bool is_row_full(QLayout* row, int checkInt){
            int buff = row->count();
            if(buff >= checkInt){
                return true;
            }
            else{
                return false;
            }
        }

        void manage_class(){
            QString selection;

            manage_classSelect->clear();

            QWidget* manage_class_widget = new QWidget;

            createNewClassEntryBtn = new QPushButton;
            deleteClassEntryBtn = new QPushButton;
            saveClassesButton = new QPushButton;

            QHBoxLayout* main_manage_class_layout = new QHBoxLayout;
            QVBoxLayout* left_control_layout = new QVBoxLayout;
            QVBoxLayout* right_control_layout = new QVBoxLayout;
            QVBoxLayout* editables_pane = new QVBoxLayout;
            QHBoxLayout* control_pane = new QHBoxLayout;

            manage_class_widget->setWindowModality(Qt::ApplicationModal);
            manage_class_widget->setMinimumSize(500,300);
            manage_class_widget->show();

            className->setEnabled(false);
            className->setText("Keine Klasse ausgewählt!");

            createNewClassEntryBtn->setText("Neue Klasse hinzufügen");
            deleteClassEntryBtn->setText("Ausgewählte Klasse löschen");
            saveClassesButton->setText("Speichern");

            //Initalize Widget
            manage_class_widget->setLayout(main_manage_class_layout);
            
            right_control_layout->addLayout(editables_pane);
            right_control_layout->addLayout(control_pane);

            left_control_layout->addWidget(manage_classSelect);
            left_control_layout->addWidget(createNewClassEntryBtn);

            main_manage_class_layout->addLayout(left_control_layout);
            main_manage_class_layout->addLayout(right_control_layout);

            editables_pane->addWidget(className);
            editables_pane->addWidget(deleteClassEntryBtn);
            editables_pane->setAlignment(editables_pane, Qt::AlignBottom|Qt::AlignHCenter);

            control_pane->setAlignment(control_pane, Qt::AlignBottom|Qt::AlignRight);
            control_pane->addWidget(saveClassesButton);

            update_listwidget(manage_classSelect, classes);

            connect(manage_classSelect, &QListView::clicked, this, [=](){getSelectedItem(manage_classSelect, selection);});
            connect(saveClassesButton, &QPushButton::clicked, this, [=](){exportClasses(classes);});
        }

        QVBoxLayout* centralLayout = new QVBoxLayout;
        QVBoxLayout* studentListLayout = new QVBoxLayout;
        QHBoxLayout* ctrlBar;
        std::vector<std::string> classes;

    private:
        QHBoxLayout* mainRow;
        QToolButton* addStudentButton;
        QPushButton* createNewClassEntryBtn;
        QPushButton* deleteClassEntryBtn;
        QPushButton* saveClassesButton;
        QPushButton* saveButton;
        QPushButton* advancedSettings;
        QComboBox* classList = new QComboBox;
        QLabel* classListLabel;
        QModelIndex classSelect_modelindex;
        QLineEdit* className = new QLineEdit;
        QListWidget* manage_classSelect = new QListWidget;

        void getSelectedItem(QListView* lv, QString ouput){
            classSelect_modelindex = manage_classSelect->currentIndex();
            if(classSelect_modelindex.isValid()){
                className->setEnabled(true);
                QListWidgetItem* selectedItem = manage_classSelect->item(classSelect_modelindex.row());
                className->setText(selectedItem->text());
            }
            else{
                className->setEnabled(false);
                className->setText("Keine Klasse ausgewählt!");
            }
        }

        void exportClasses(std::vector<std::string> classNameList){
            //If DB exists, remove it. Otherwise pass.
            std::string dir = "./data/studentmanager-classes.db";
            std::FILE* file = std::fopen(dir.data(), "r");
            if(file){
                std::fclose(file);
                std::remove(dir.data());
            };

            sqlite3* DB;
            sqlite3_stmt* DB_STATEMENT;
            char* errmsg;

            char* SQL_COMMAND = "CREATE TABLE CLASSES(NUMBER INT, NAME TEXT);";

            int rc = sqlite3_open("./data/studentmanager-classes.db", &DB);

            //ERROR HANDLING
            if(rc != SQLITE_OK){
                showExportClassesErrBox("SAStudentManager, exportClasses, 0");
            }

            rc = sqlite3_exec(DB, SQL_COMMAND, NULL, 0, &errmsg);

            if(rc != SQLITE_OK){
                showExportClassesErrBox("SAStudentManager, exportClasses, 1");
            }

            for(int i = 0; i < classNameList.size(); i++){
                rc = sqlite3_prepare_v2(DB, "INSERT INTO CLASSES(NUMBER, NAME) VALUES (?,?)", -1, &DB_STATEMENT, NULL);
                if(rc != SQLITE_OK){
                    showExportClassesErrBox("SAStudentManager, exportClasses, 2");
                    break;
                }

                sqlite3_bind_int(DB_STATEMENT, 1, i);
                sqlite3_bind_text(DB_STATEMENT, 2, classNameList.at(i).data(), -1, SQLITE_STATIC);

                rc = sqlite3_step(DB_STATEMENT);

                if(rc != SQLITE_DONE){
                    showExportClassesErrBox("SAStudentManager, exportClasses 3");
                    break;
                }

                sqlite3_finalize(DB_STATEMENT);
            }
            
            sqlite3_close(DB);
        }

        std::vector<std::string> importClasses(){
            std::vector<std::string> bufferVector;

            sqlite3* DB;
            sqlite3_stmt* DB_STATEMENT;

            int rc = sqlite3_open("./data/studentmanager-classes.db", &DB);
            if(rc != SQLITE_OK){
                QMessageBox msg;
                msg.setWindowTitle("Student Access | Error");
                msg.setInformativeText("Error during the import: Could not import from table CLASSES in file ./data/studentmanager-classes.db. Please read the documentation to resolve the issues or create a nug report on GitLab/. ERROR: SAStudentManager, importClasses, 0");
                msg.exec();
            }

            rc = sqlite3_prepare_v2(DB, "SELECT * FROM CLASSES", -1, &DB_STATEMENT, NULL);
            if(rc != SQLITE_OK){
                QMessageBox msg;
                msg.setWindowTitle("Student Access | Error");
                msg.setInformativeText("Error during the import: Could not import from table CLASSES in file ./data/studentmanager-classes.db. Please read the documentation to resolve the issues or create a nug report on GitLab/. ERROR: SAStudentManager, importClasses, 1");
                msg.exec();
            }

            while(sqlite3_step(DB_STATEMENT) == SQLITE_ROW){
                bufferVector.push_back(reinterpret_cast<const char*>(sqlite3_column_text(DB_STATEMENT, 1)));
            }

            return bufferVector;
        }

        void showExportClassesErrBox(QString ERR){
            QMessageBox msg;
            msg.setWindowTitle("Student Access | Error");
            msg.setInformativeText("Error during the export: Could not export classes into ./data/studentmanager-classes.db. Please read the documentation to resolve the issues or create a bug report on GitLab/Hub. ERROR: " + ERR);
            msg.exec();
        }

        int getPosition(std::vector<std::string> vector, std::string text){
            //Check if variable vector has variable text in it. If true, return current positon. Ergo: getPosition 

            for(int i=0; i<vector.size(); i++){
                if(text == vector.at(i)){
                    return i;
                    break;
                }
                else{
                    return -1;
                    break;
                }
            }
        }

        void update_listwidget(QListWidget* qlw, std::vector<std::string> vecstr){
            for(int i=0; i<vecstr.size(); i++){
                qlw->addItem(QString::fromStdString(vecstr.at(i)));
            }
        }

        void rename_class(std::string textbefore, std::string textafter, std::vector<std::string> vecstr){
            int position = getPosition(vecstr, textbefore);

            if(getPosition(vecstr, textafter) != -1){
                vecstr[position] = textafter;
            } else {
                QMessageBox msg;
                msg.setInformativeText("Dieser Name existiert schon. Wählen Sie bitte einen anderen Namen!");
                msg.setWindowTitle("Student Access | Error");
            }
        }
};

#endif //SASTUDENT_MANAGER_HPP