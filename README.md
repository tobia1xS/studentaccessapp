# Student Access

## Supported Languages

- German [==========]100%
- English [          ] 0%
- Spanish [          ] 0%

## Version 0.1
### Features
- Timetable

## How to build
```
NO DATA YET
```

## Tested Platforms
- Linux (Debian)

## Used Frameworks
- [Qt](https://www.qt.io/)
- [sqlite](https://www.sqlite.org/index.html)

## License
LGPL Version 2.1

## Other Information
**As much as possible C/C++**

Author: Tobias S. <br >
Developers: Tobias S. <br >

**MADE IN AUSTRIA**

## First verse of the austrian national anthem

Land der Berge, Land der Strome, <br >
Land der Äcker, Land der Dome, <br >
Land der Hämmer, zukunftsreich! <br >
Heimat großer Töchter und Söhne, <br >
Volk, begnandet für das Schöne, <br >
Vielgerühmtes Österreich! <br >
----Wolfgang Amadeus Mozart, Paula Preradović
