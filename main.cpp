#include "mainappwindow.hpp"

int main(int argc, char *argv[]){
	QApplication app(argc, argv);

	MainAppWindow mainAppWindow;
	mainAppWindow.show();

	return app.exec();
}
