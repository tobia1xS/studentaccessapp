#include <QApplication>
#include <QMainWindow>
#include "mainappwindow.hpp"
#include <QLayout>
#include <QAbstractButton>
#include <QPushButton>
#include <QToolButton>
#include <QSizePolicy>
#include <QWidget>
#include <QMessageBox>
#include <SATimetable.hpp>
#include <SAStudentManager.hpp>

//SAStudentManager *student_mgr;

MainAppWindow::MainAppWindow(QWidget* parent) : QMainWindow(parent){
	this->setWindowTitle("Student Access");
	this->setMinimumSize(1500,800);

	QWidget *cW = new QWidget;

	this->setCentralWidget(cW);

	QVBoxLayout* mainLayout = new QVBoxLayout;

	cW->setLayout(mainLayout);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(0,0,0,0);

	//Upper Layout of mainLayout
	QHBoxLayout* taskLayout = new QHBoxLayout;
	mainLayout->addLayout(taskLayout);

	//Code everything on the upper half of the window here

	//Topbar
	auto *taskbar = new QWidget;
	auto *taskbarLayout = new QHBoxLayout;
	taskbar->setStyleSheet("QWidget{background-color:rgb(86,85,84);}");
	taskbar->setMaximumHeight(30);
	
	//Corner Buffer
	auto* cornerBuffer = new QWidget;
	auto* shutdownButton = new QPushButton;
	QHBoxLayout *bufferLayout = new QHBoxLayout;
	QPixmap shutdownPM("./graphics/shutdown-corner-buffer.png");
	QIcon shutdownIcon(shutdownPM);
	cornerBuffer->setStyleSheet("QWidget{background-color: rgb(224,122,95);}");
	cornerBuffer->setMaximumSize(70, 30);
	cornerBuffer->setLayout(bufferLayout);
	shutdownButton->setIcon(shutdownIcon);
	shutdownButton->setIconSize(QSize(70,30));
	shutdownButton->setMinimumSize(70,30);
	shutdownButton->setStyleSheet("QToolButton{border:none;background-color:none;}");
	bufferLayout->setContentsMargins(0,0,0,0);
	bufferLayout->addWidget(shutdownButton);
	
	taskLayout->addWidget(cornerBuffer);
	taskLayout->addWidget(taskbar);
	
	//Lower Layout of mainLayout
	QHBoxLayout *workLayout = new QHBoxLayout;
	mainLayout->addLayout(workLayout);
	workLayout->setContentsMargins(0,0,0,0);
	workLayout->setSpacing(0);

	//Code everything on the lower half of the window here
	
	//Sidebar
	auto *applicationBar = new QWidget;
	QVBoxLayout* applicationBarLayout = new QVBoxLayout;
	QVBoxLayout* applicationBarLayoutToggles = new QVBoxLayout;
	applicationBar->setStyleSheet("QWidget{background-color: rgb(86,85,84);}");
	applicationBar->setMaximumWidth(70);
	applicationBar->setMinimumWidth(70);
	applicationBar->setLayout(applicationBarLayout);
	applicationBarLayout->setContentsMargins(0,0,0,0);
	applicationBarLayout->setSpacing(0);

	applicationBarLayoutToggles->setContentsMargins(0,0,0,0);
	applicationBarLayoutToggles->setSpacing(20);
	
	workLayout->addWidget(applicationBar);
	applicationBarLayout->addLayout(applicationBarLayoutToggles);

	//Widget Toggles
	timetableToggle = new QPushButton;
	QPixmap timetablePM("./graphics/timetable.png");
	QIcon timetableIcon(timetablePM);
	timetableToggle->setMinimumSize(50,50);
	timetableToggle->setMaximumSize(50,50);
	timetableToggle->setStyleSheet("QPushButton{background-color: none; border: none;}");
	timetableToggle->setIcon(timetableIcon);
	timetableToggle->setIconSize(QSize(50,50));
	timetableToggle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	studentsToggle = new QPushButton;
	QPixmap studentsPM("./graphics/student.png");
	QIcon studentsIcon(studentsPM);
	studentsToggle->setMinimumSize(50,50);
	studentsToggle->setMaximumSize(50,50);
	studentsToggle->setStyleSheet("QPushButton{background-color: none; border: none;}");
	studentsToggle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	studentsToggle->setIcon(studentsIcon);
	studentsToggle->setIconSize(QSize(50,50));
	

	applicationBarLayoutToggles->addWidget(studentsToggle);
	applicationBarLayoutToggles->addWidget(timetableToggle);

	applicationBarLayoutToggles->setAlignment(studentsToggle, Qt::AlignHCenter);
	applicationBarLayoutToggles->setAlignment(timetableToggle, Qt::AlignHCenter);

	//Workspace
	auto* workspace = new QWidget;
	workspaceLayout = new QHBoxLayout;
	workspace->setStyleSheet("QWidget{background-color: #d6f7fd;}");
	workspace->setLayout(workspaceLayout);

	workLayout->addWidget(workspace);

	//Initalize apps
	timetable = new SATimetable;
	timetable->initWidget(this);

	student_mgr = new SAStudentManager;
	student_mgr->initWidget();

	//Connections

	connect(shutdownButton, &QPushButton::clicked, this, &MainAppWindow::shutdownAction);
	connect(timetableToggle, &QPushButton::clicked, this, [=](){toggleWidget(workspaceLayout, timetable);});
	connect(studentsToggle, &QPushButton::clicked, this, [=](){toggleWidget(workspaceLayout, student_mgr);});
}
//Slots

void MainAppWindow::shutdownAction(){
	QMessageBox msgb;
	msgb.setWindowTitle("Student Access - Warnung");
	msgb.setInformativeText("Möchten Sie Student Access wirklich beenden?\n(Alle nicht gespeicherten Änderungen gehen dabei verloren!)");
	msgb.setStyleSheet("QLabel{min-width:400px;}");
	
	QPushButton* yesBtn = msgb.addButton(tr("Ja"), QMessageBox::ActionRole);
	QPushButton* noBtn = msgb.addButton(QMessageBox::Abort);
	noBtn->setText("Nein");

	msgb.exec();

	if(msgb.clickedButton() == yesBtn){
		this->close();
	}

	else{
		msgb.close();
	}
}

void MainAppWindow::removeAllWidgets(QLayout* layout){
	QLayoutItem *item;
	while((item = layout->takeAt(0)) != nullptr){
		QWidget *w = item->widget();
		if(w != nullptr){
			workspaceLayout->removeWidget(w);
			w->hide();
		}
	}
}

void MainAppWindow::toggleWidget(QLayout* layout, QWidget* widget){
	if(!widget->isVisible()){
		removeAllWidgets(layout);
		layout->addWidget(widget);
		widget->show();
	}
	else{
		removeAllWidgets(layout);
	}
}