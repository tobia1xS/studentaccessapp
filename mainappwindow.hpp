#include <QApplication>
#include <QMainWindow>
#include <QWidget>
#include "SATimetable.hpp"
#include "SAStudentManager.hpp"

class MainAppWindow : public QMainWindow{
	Q_OBJECT

	public:
		MainAppWindow(QWidget *parent = nullptr);

		SATimetable *timetable;
		SAStudentManager* student_mgr;

		QPushButton* timetableToggle;
		QPushButton* studentsToggle;
		QHBoxLayout* workspaceLayout;
		bool timetableActive = false;
	//
	private:

		void removeAllWidgets(QLayout *layout);
		void toggleWidget(QLayout* layout, QWidget* widget);

		//Slots
		void shutdownAction();
};
