#ifndef SATIMETABLE_HPP
#define SATIMETABLE_HPP

#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QToolButton>
#include <QPushButton>
#include <QFontDatabase>
#include <QFont>
#include <QColorDialog>
#include <QSpinBox>
#include <QSizePolicy>
#include <QMessageBox>
#include <QSpacerItem>
#include <QTextEdit>
#include <QLineEdit>
#include <QRegularExpression>
#include <QPainter>
#include <vector>
#include <iostream>
#include "include/SQLite/sqlite3.h"
#include <cstdio>
#include <algorithm>

class SATimetable : public QWidget{
    Q_OBJECT

    public:

        explicit SATimetable(QWidget *parent = nullptr){
            centralLayout = new QHBoxLayout;
            this->setLayout(centralLayout);
            centralLayout->setAlignment(Qt::AlignTop);
        }

        QToolButton* addLessonBtns[7];
        QWidget *wrapperWidget[7];
        int index;

        void initWidget(QWidget* program){
            //Add Panes
            QWidget* panes[7];
            QPushButton* exportToDatabase = new QPushButton;
            QHBoxLayout* paneLayouts[7];
            QVBoxLayout* controls = new QVBoxLayout;
            QLabel* paneLabels[7];
            QFontDatabase::addApplicationFont("./fonts/Quicksand-Regular.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-Medium.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-SemiBold.ttf");

            std::vector<std::string> days = {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"};

            QPixmap newIconShadePM("./graphics/new-shade.png");
            QIcon newIconShade(newIconShadePM);

            for(int i=0;i<7;i++){
                panes[i] = new QWidget();
                paneLabels[i] = new QLabel;
                paneLayouts[i] = new QHBoxLayout;
                row[i] = new QVBoxLayout;
                wrapperWidget[i] = new QWidget;
                addLessonBtns[i] = new QToolButton;

                panes[i]->setMinimumSize(160,50);
                panes[i]->setMaximumSize(160,50);
                panes[i]->setLayout(paneLayouts[i]);
                paneLabels[i]->setText(QString::fromStdString(days.at(i)));
                paneLabels[i]->setFont(QFont("Quicksand SemiBold", 18));
                addLessonBtns[i] = new QToolButton;
                addLessonBtns[i]->setMinimumSize(160,50);
                addLessonBtns[i]->setMaximumSize(160, 50);
                addLessonBtns[i]->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
                addLessonBtns[i]->setIcon(newIconShade);
                addLessonBtns[i]->setText("Stunde hinzufügen");
                addLessonBtns[i]->setStyleSheet("QToolButton{background-color: #c6c6c6; color: black; border:none; border-radius: 5px;}");
                addLessonBtns[i]->setFont(QFont("Quicksand Medium", 12));
                addLessonBtns[i]->setIconSize(QSize(23,23));
                paneLayouts[i]->setAlignment(Qt::AlignVCenter|Qt::AlignHCenter);
                paneLayouts[i]->addWidget(paneLabels[i]);
                panes[i]->setStyleSheet("QWidget{background-color:white; border: none; border-radius: 5px; color:black;}");
                row[i]->setAlignment(Qt::AlignTop);
                row[i]->addWidget(panes[i]);
                row[i]->addWidget(addLessonBtns[i]);
                centralLayout->addWidget(wrapperWidget[i]);
                wrapperWidget[i]->setLayout(row[i]);

                connect(addLessonBtns[i], &QToolButton::clicked, this, [this, i](){addLesson(i, nullptr, nullptr, NULL);});
            }

            //Create the save button row
            controls->setAlignment(Qt::AlignRight|Qt::AlignTop);

            exportToDatabase->setText("Speichern");
            exportToDatabase->setStyleSheet("QPushButton{background-color:white; border:none; border-radius:5px;color:black;}");
            exportToDatabase->setFont(QFont("Quicksand Regular", 12));
            exportToDatabase->setFixedSize(100,50);
            controls->addWidget(exportToDatabase);
            centralLayout->addLayout(controls);

            connect(exportToDatabase, &QPushButton::clicked, this, [=](){exportDatabase(program);});

            loadDatabase();
        }

        void addLesson(int dayIndex, QString provided_stylesheet, QString provided_name, int provided_fontsize){
            /*Monday=0
            ...
            Sunday=6*/

            QFontDatabase::addApplicationFont("./fonts/Quicksand-Regular.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-Medium.ttf");
            QFontDatabase::addApplicationFont("./fonts/Quicksand-SemiBold.ttf");

            lessonWidget = new QPushButton;

            //If data is provided, set it to it. This is due to loading from the database. I can easliy load everything with one method with this.
            if(provided_stylesheet==nullptr){
                lessonWidget->setStyleSheet("QPushButton{background-color:#ffffff;border:none;border-radius:5px;color:#000000;}");
            }
            else{
                lessonWidget->setStyleSheet(provided_stylesheet);
            }

            lessonWidget->setMinimumSize(160,50);
            lessonWidget->setMaximumSize(160,50);

            if(provided_fontsize==NULL){
                QFont bufffont;
                bufffont = lessonWidget->font();
                bufffont.setPointSize(15);
                lessonWidget->setFont(bufffont);
            }
            else{
                QFont bufffont;
                bufffont = lessonWidget->font();
                bufffont.setPointSize(provided_fontsize);
                lessonWidget->setFont(bufffont);
            }

            lessonWidget->setFont(QFont("Quicksand Medium", lessonWidget->font().pointSize()));

            if(provided_name==nullptr){
                lessonWidget->setText("Neue Stunde");
            }
            else{
                lessonWidget->setText(provided_name);
            }

            //Insert row before the last widget
            row[dayIndex]->insertWidget(row[dayIndex]->count() - 1, lessonWidget);

            connect(lessonWidget, &QPushButton::clicked, this, [=](){editLessonDialog();});

            lessonCount++;
        }

        void editLessonDialog(){
            lessonNameTypefield = new QLineEdit;
            fontSizeSB = new QSpinBox;
            QPushButton* lesson = qobject_cast<QPushButton*>(sender());

            editDiag = new QWidget;
            fontSizeSB->setValue(lessonWidget->font().pointSize());
            primaryColourPreview = new QPushButton;
            secondaryColourPreview = new QPushButton;
            QPushButton* exitAndSave = new QPushButton;
            QPushButton* advancedEditing = new QPushButton;
            QPushButton* deleteLesson = new QPushButton;
            QHBoxLayout* centralEditLayout = new QHBoxLayout;
            QHBoxLayout* colourPreviewLayout = new QHBoxLayout;
            QVBoxLayout* basicEditPane = new QVBoxLayout;
            QVBoxLayout* colourEditPane = new QVBoxLayout;

            editDiag->setWindowModality(Qt::ApplicationModal);
            editDiag->setWindowTitle("Student Access | Stunde Bearbeiten");
            editDiag->setMinimumSize(700,500);
            editDiag->setLayout(centralEditLayout);

            centralEditLayout->setSpacing(20);
            centralEditLayout->addLayout(basicEditPane);
            centralEditLayout->addLayout(colourEditPane);

            basicEditPane->addWidget(lessonNameTypefield);
            basicEditPane->addWidget(fontSizeSB);
            basicEditPane->addWidget(deleteLesson);

            fontSizeSB->setValue(lesson->font().pointSize());

            colourEditPane->addLayout(colourPreviewLayout);

            colourPreviewLayout->addWidget(primaryColourPreview);
            colourPreviewLayout->addWidget(secondaryColourPreview);

            lessonNameTypefield->setText(lesson->text());
            lessonNameTypefield->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

            deleteLesson->setText("Stunde Löschen");

            //Get the style sheet of the current lesson widget
            QString oldCSS = lesson->styleSheet();

            //Get the border radius of the current CSS and extract it, then put it to an int
            QRegularExpression re("border-radius:\\s*(\\d+)");
            QRegularExpressionMatch borderRadiusMatch = re.match(oldCSS);

            if(borderRadiusMatch.hasMatch()){
                QString currentBorderRadiusString = borderRadiusMatch.captured(1);
                currentBorderRadius = currentBorderRadiusString.toInt();
            }

            //Get The Colours of the Text (Secondary) and the Background (Primary) and then set it into the the Colourpreviews
            int hashtagPrimaryIndex = oldCSS.indexOf("#");
            int hashtagSecondaryIndex = oldCSS.indexOf("#", oldCSS.indexOf("#") + 1);

            if(hashtagPrimaryIndex !=-1&&hashtagPrimaryIndex + 7<=oldCSS.length()){
                currentPrimaryColour = oldCSS.mid(hashtagPrimaryIndex, 7);
                currentSecondaryColour = oldCSS.mid(hashtagSecondaryIndex, 7);
            }

            newPrimaryCSS = "QPushButton{background-color: " + currentPrimaryColour + "; border:2px solid black; border-radius:5px;}";
            primaryColourPreview->setStyleSheet(newPrimaryCSS);
            primaryColourPreview->setMaximumSize(100,100);
            primaryColourPreview->setMinimumSize(100,100);
            
            newSecondaryCSS = "QPushButton{background-color: " + currentSecondaryColour + "; border: 2px solid black; border-radius:5px;}";
            secondaryColourPreview->setStyleSheet(newSecondaryCSS);
            secondaryColourPreview->setMaximumSize(100,100);
            secondaryColourPreview->setMinimumSize(100,100);

            colourEditPane->addWidget(advancedEditing);
            advancedEditing->setText("Erweiterte Einstellungen...");
            
            colourEditPane->addWidget(exitAndSave);
            exitAndSave->setText("Speichern und Verlassen");
            exitAndSave->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

            editDiag->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

            connect(exitAndSave, &QPushButton::clicked, this, [=](){exitAndSaveSlot(editDiag, lesson);});
            connect(deleteLesson, &QPushButton::clicked, this, [=](){deleteLessonSlot(lesson->parentWidget()->layout(), lesson);});
            connect(primaryColourPreview, &QPushButton::clicked, this, [=](){openColourDialog(0, lesson);/*0=Primary;1=Secondary*/});
            connect(secondaryColourPreview, &QPushButton::clicked, this, [=](){openColourDialog(1, lesson);});
            connect(advancedEditing, &QPushButton::clicked, this, [=](){openCSSDialog(lesson);});

            editDiag->show();
        }

        void exitAndSaveSlot(QWidget* widget, QPushButton* saveToLesson){
            fontSize=fontSizeSB->value();
            QFont btnFont;
            btnFont = lessonWidget->font();
            btnFont.setPointSize(fontSize);
            saveToLesson->setText(lessonNameTypefield->text()); //Save New Name To Label
            saveToLesson->setFont(btnFont);

            widget->close();
        }

        void deleteLessonSlot(QLayout* parent, QPushButton* lessonToDelete){
            QMessageBox *msg = new QMessageBox;
            msg->setWindowTitle("Student Access | Stunde Löschen");
            msg->setInformativeText("Möchtest du diese Stunde wirklich löschen?");
            msg->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msg->setDefaultButton(QMessageBox::Yes);
            int result = msg->exec();

            if(result == QMessageBox::Yes){
                parent->removeWidget(lessonToDelete);
                lessonToDelete->deleteLater();
                editDiag->close();
                msg->accept();
                msg->deleteLater();
            }
            else{
                msg->reject();
                msg->deleteLater();
            }
        }

        void openColourDialog(int primScndIndex, QPushButton* lessonWidgetToEdit){
            /*primScndIndex
            0 = Primary
            1 = Secondary*/

            QColor oldHEXColour;
            QColor newColour;

            if(primScndIndex == 0){
                oldHEXColour.setNamedColor(currentPrimaryColour);
            }
            else{
                oldHEXColour.setNamedColor(currentSecondaryColour);
            }

            colourDiag = new QColorDialog;
            colourDiag->setWindowModality(Qt::ApplicationModal);
            colourDiag->setCurrentColor(oldHEXColour);

            if(colourDiag->exec() == QDialog::Accepted){
                newColour = colourDiag->selectedColor();
            }

            if(primScndIndex == 0){
                currentPrimaryColour = newColour.name();
            }
            else{
                currentSecondaryColour = newColour.name();
            }

            lessonWidgetToEdit->setStyleSheet("QPushButton{background-color:" + currentPrimaryColour + ";border:none;border-radius:" + QString::number(currentBorderRadius) + "px;color:" + currentSecondaryColour + ";}");
            primaryColourPreview->setStyleSheet("QPushButton{background-color: " + currentPrimaryColour + "; border: 2px solid black; border-radius: 5px;}");
            secondaryColourPreview->setStyleSheet("QPushButton{background-color: " + currentSecondaryColour + "; border: 2px solid black; border-radius: 5px;}");
        }

        void openCSSDialog(QWidget* lesson){
            QWidget* CSSDiag = new QWidget;
            CSSBox = new QTextEdit;
            QVBoxLayout* CSSDiagLayout = new QVBoxLayout;
            QHBoxLayout* UpperLayout = new QHBoxLayout;
            QPushButton* setCSS = new QPushButton;

            QMessageBox msg;
            msg.setWindowTitle("Student Access | Information");
            msg.setMinimumSize(500,300);
            msg.setMaximumSize(500,300);
            msg.setInformativeText("Bitte lassen Sie keine Leerzeichen im CSS Code!\nBearbeitung auf eigene Gefahr!");
            msg.exec();

            CSSDiag->setMinimumSize(500,300);
            CSSDiag->setWindowModality(Qt::ApplicationModal);

            UpperLayout->addWidget(new QLabel("Cascading Style Sheet (CSS) bearbeitung"));
            UpperLayout->addSpacerItem(new QSpacerItem(10,10, QSizePolicy::Expanding, QSizePolicy::Fixed)); //Left: Text, Right: Button
            UpperLayout->addWidget(setCSS);

            CSSBox->setText(lesson->styleSheet());
            
            CSSDiagLayout->addLayout(UpperLayout);
            CSSDiagLayout->addWidget(CSSBox);

            setCSS->setText("Festlegen");

            CSSDiag->setLayout(CSSDiagLayout);
            CSSDiag->setWindowTitle("Student Access | Stunde Bearbeiten > Eigenes CSS Festlegen");

            CSSDiag->show();

            connect(setCSS, &QPushButton::clicked, this, [=](){setLessonWidgetCSS(CSSBox, lesson);});
        }

        void setLessonWidgetCSS(QTextEdit* input, QWidget* lessonWidget){
            QString CSSString = input->toPlainText();
            
            QRegularExpression re("border-radius:\\s*(\\d+)");
            QRegularExpressionMatch borderRadiusMatch = re.match(CSSString);

            if(borderRadiusMatch.hasMatch()){
                QString currentBorderRadiusString = borderRadiusMatch.captured(1);
                currentBorderRadius = currentBorderRadiusString.toInt();
            }

            lessonWidget->setStyleSheet(CSSString);

            int indexPrimary = CSSString.indexOf("#");
            int indexSecondary = CSSString.indexOf("#", CSSString.indexOf("#") + 1);
            int indexOfBorderRadius = CSSString.indexOf("border-radius:");

            currentPrimaryColour = CSSString.mid(indexPrimary, 7);
            currentSecondaryColour = CSSString.mid(indexSecondary, 7);

            primaryColourPreview->setStyleSheet("QPushButton{background-color: " + currentPrimaryColour + "; border: 2px solid black; border-radius: 5px;}");
            secondaryColourPreview->setStyleSheet("QPushButton{background-color: " + currentSecondaryColour + "; border: 2px solid black; border-radius: 5px;}");
        }

        void exportDatabase(QWidget* mainProgram){
            QMessageBox msgb;
            msgb.setWindowTitle("Student Access | Warnung");
            msgb.setInformativeText("Wenn Sie jetzt speichern, gehen die vorherigen Daten verloren. Möchten Sie trotzdem fortfahren?");
            msgb.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgb.setDefaultButton(QMessageBox::No);
            msgb.setStyleSheet("QLabel{min-width:600px;min-height:15px;}");

            if(msgb.exec() != QMessageBox::No){
                std::vector<std::string> names;
                std::vector<int> days;
                std::vector<std::string> stylesheets;
                std::vector<int> fontsizes;
                sqlite3* timetable_db;

                for(int i=0;i<7;i++){
                        QWidget* buffWrapperWidget = centralLayout->itemAt(i)->widget();
                        QLayout* buffLayout = buffWrapperWidget->layout();
                        for(int y=1;y<buffLayout->count()-1;y++){
                            QPushButton *buffbtn = qobject_cast<QPushButton*>(buffLayout->itemAt(y)->widget());
                            QString name = buffbtn->text();
                            QString ss = buffbtn->styleSheet();
                            int fontsize = buffbtn->font().pointSize();
                            fontsizes.push_back(fontsize);
                            names.push_back(name.toStdString());
                            days.push_back(i);
                            stylesheets.push_back(ss.toStdString());
                        }
                    }

                    if(days.size() == 0){
                        displayMSGB("Student Access | Speicherprozess", "Du hast keine Stunden erstellt! Es gibt nichts zu speichern!", 500,200);
                    }
                    else if (days.size() != 0){
                        std::string dir = "./data/timetable.db";
                        std::FILE* file = std::fopen(dir.data(), "r");
                        if(file){
                            std::fclose(file);
                            std::remove(dir.data());
                        };

                        int retmsg = sqlite3_open("./data/timetable.db", &timetable_db);

                        if(retmsg != SQLITE_OK){
                            displayMSGB("Student Access Writing Error", "Error while trying to write to database 'timetable.db' located in './data/' at sqlite3_open(...);", 500,300);
                            mainProgram->close();
                        }

                        char* SQL_STATEMENT = "CREATE TABLE TIMETABLE(DAY INT, NAME TEXT, STYLESHEET TEXT, FONTSIZE INT);";
                        char* errmsg;
                        retmsg = sqlite3_exec(timetable_db, SQL_STATEMENT, NULL, 0, &errmsg);

                        if(retmsg != SQLITE_OK){
                            displayMSGB("Student Access Writing Error", "Error while trying to write to database 'timetable.db' located in './data/' at sqlite3_exec(...,SQL_STATEMENT,...);", 500,300);
                            mainProgram->close();
                        }

                        for(int i=0; i<names.size(); i++){
                            sqlite3_stmt* SQL_PREP_STATEMENT;
                            
                            retmsg = sqlite3_prepare_v2(timetable_db, "INSERT INTO TIMETABLE (DAY, NAME, STYLESHEET, FONTSIZE) VALUES (?,?,?,?)", -1, &SQL_PREP_STATEMENT, NULL);
                            if(retmsg != SQLITE_OK){
                                displayMSGB("Student Access Writing Error", "Error while trying to write to database 'timetable.db' located in './data/' at sqlite3_prepare_v2(...);", 500,300);
                                mainProgram->close();
                                break;
                            }

                            sqlite3_bind_int(SQL_PREP_STATEMENT, 1, days.at(i));
                            sqlite3_bind_text(SQL_PREP_STATEMENT, 2, names.at(i).data(), -1, SQLITE_STATIC);
                            sqlite3_bind_text(SQL_PREP_STATEMENT, 3, stylesheets.at(i).data(), -1, SQLITE_STATIC);
                            sqlite3_bind_int(SQL_PREP_STATEMENT, 4, fontsizes.at(i));

                            retmsg = sqlite3_step(SQL_PREP_STATEMENT);
                            if(retmsg != SQLITE_DONE){
                                displayMSGB("Student Access Writing Error", "Error while trying to write to database 'timetable.db' located in './data/' at sqlite3_step(...);", 500,300);
                                mainProgram->close();
                                break;
                            }
                            sqlite3_finalize(SQL_PREP_STATEMENT);
                        }
                        sqlite3_close(timetable_db);
                }
            }
        }

        int loadDatabase(){
            /*
            RETURN VALUES:
            ---------------------S
            1---PATH DOESN'T EXIST
            0--LOADED SUCESSFULLLY
            */
            std::string dir = "./data/timetable.db";
            std::FILE* file = std::fopen(dir.data(), "r");

            if(file){
                std::vector<int> days;
                std::vector<std::string> names;
                std::vector<std::string> stylesheets;
                std::vector<int> fontsizes;

                sqlite3* DB;
                sqlite3_stmt* DATABASE_STATEMENT;

                int retmsg = sqlite3_open("./data/timetable.db", &DB);
                if(retmsg != SQLITE_OK){
                    displayMSGB("Student Access | Error", "Error while trying to load from database 'timetable.db' from './data/' in file SATimetable.hpp at method loadDatabase(){...} at sqlite3_open()", 800, 300);
                }

                retmsg = sqlite3_prepare_v2(DB, "SELECT * FROM TIMETABLE", -1, &DATABASE_STATEMENT, NULL);
                if(retmsg != SQLITE_OK){
                    displayMSGB("Student Access | Error", "Error while trying to load from database 'timetable.db' from './data/' in file SATimetable.hpp at method loadDatabase(){...} at sqlite3_prepare_v2()", 800, 300);
                }

                while (sqlite3_step(DATABASE_STATEMENT) == SQLITE_ROW) {
                    int day = sqlite3_column_int(DATABASE_STATEMENT, 0);
                    const unsigned char* name = sqlite3_column_text(DATABASE_STATEMENT, 1);
                    const unsigned char* stylesheet = sqlite3_column_text(DATABASE_STATEMENT, 2);
                    int fontsize = sqlite3_column_int(DATABASE_STATEMENT, 3);

                    days.push_back(day);
                    names.push_back(std::string(reinterpret_cast<const char*>(name)));
                    stylesheets.push_back(std::string(reinterpret_cast<const char*>(stylesheet)));
                    fontsizes.push_back(fontsize);
                }

                sqlite3_finalize(DATABASE_STATEMENT);
                sqlite3_close(DB);

                for(int i=0;i<days.size(); i++){
                    addLesson(days.at(i), QString::fromStdString(stylesheets.at(i)), QString::fromStdString(names.at(i)), fontsizes.at(i));
                }

                std::fclose(file);

                return 0;
            }
            else{
                return 1;
            }
        }

        void displayMSGB(QString windowTitle, QString infoText, int fixedW, int fixedH){
            QString buffsytlesheet = "QLabel{min-width: " + QString::fromStdString(std::to_string(fixedW)) + "px; max-width: " + QString::fromStdString(std::to_string(fixedW)) + "px; min-height: " + QString::fromStdString(std::to_string(fixedH)) + "px; max-height: " + QString::fromStdString(std::to_string(fixedH)) + "px;}";
            QMessageBox msg;
            msg.setStyleSheet(buffsytlesheet);
            msg.setWindowTitle(windowTitle);
            msg.setInformativeText(infoText);
            msg.exec();
        }

        QPushButton *lessonWidget;
        int lessonCount;
        int fontSize;
        int currentBorderRadius;
        QString currentPrimaryColour;
        QString currentSecondaryColour;
        QString newPrimaryColour;
        QString newSecondaryColour;
        QString newPrimaryCSS;
        QString newSecondaryCSS;
        QVBoxLayout* row[7];

    private:

        QPushButton* primaryColourPreview;
        QPushButton* secondaryColourPreview;
        QHBoxLayout *centralLayout;
        QLineEdit* lessonNameTypefield;
        QSpinBox* fontSizeSB;
        QTextEdit* CSSBox;
        QWidget* editDiag;
        QColorDialog* colourDiag;
};

#endif //SATIMETABLE_H